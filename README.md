# docker-suite

A Docker image to rule them all. Includes:

* `docker`
* `docker-compose`
* `docker-machine`